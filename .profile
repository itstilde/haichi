shopt -s histappend                         # append to history, don't overwrite it
shopt -s checkwinsize                       # ensures correct wrapping after resizing window
shopt -s nocaseglob                         # Case-insensitive globbing (used in pathname expansion)
shopt -s cdspell                            # Autocorrect typos in path names when using `cd`
#shopt -s extglob                           # Extended pattern matching features
shopt -s hostcomplete                       # Hostname expansion
shopt -s no_empty_cmd_completion            # stops cmd suggestion on empty prompy?

ulimit -S -c 0                              # disable core dumps
set -o notify                               # notify of completed background jobs immediately

# Bash completion extended (single press on partial or no-completion)
set show-all-if-ambiguous on

#TODO ROTATING BASH HISTORY LOGS
#[ -d ~/.bhistory ] || mkdir ~/.bhistory
#[ -d ~/.bhistory ] && chmod 0700 ~/.bhistory

export HISTFILE="${HOME}/.bash_history"
export HISTTIMEFORMAT='%Y-%m-%d %H:%M.%S | '   # Log the time on that shit
export HISTFILESIZE=10240                      # mad huge history count
export HISTSIZE=10240                          # mad huge file size
export HISTCONTROL=ignoredups:erasedups        # ignore duplicaets
export HISTIGNORE="ls:exit:history:[bf]g:jobs" # Ignore some useless shit

export PAGER=less -r
export PYTHONIOENCODING='utf-8'

# Enable some Bash 4 features when possible:
# * `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
# * Recursive globbing, e.g. `echo **/*.txt`
for option in autocd globstar; do
    shopt -s "$option" 2> /dev/null;
done;

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2 | tr ' ' '\n')" scp sftp ssh;


export CLICOLOR=YES # colours on OSX
# Make bash history work across windows/tmux panes
# Not sure if this will mess around with multiple users logged into the same user
# PROMPT_COMMAND="history -a;history -c;history -r;$PROMPT_COMMAND"

# auto completion
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# create local instance of .bash_profile.local & bashrc.local for overrides if needed
[[ ! -f "$HOME/.profile.local" ]] && touch "$HOME/.profile.local"
[[ ! -f "$HOME/.bashrc.local" ]] && touch "$HOME/.bashrc.local"

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin directories
PATH="$HOME/bin:$PATH"
